# Configuration for DEVELOPMENT 
# base image
FROM node:9.6.1

# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY yarn.lock /usr/src/app/yarn.lock
COPY package.json /usr/src/app/package.json

RUN yarn
RUN yarn add global react-scripts@1.1.1

COPY . .

EXPOSE 3000

# start app
CMD ["yarn", "start"]
import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'

import Background from './Background'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<Background />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<Background selectedComponents={initObject}  />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('renders the visualizaton composed of 6 images ', () => {
    const wrapper = mount(<Background selectedComponents={initObject}  />)
    expect(wrapper.find('img').length).toEqual(6)
  })

  it('renders the visualizaton composed of 5 images to uncover the interior of the lift ', () => {
    const wrapper = mount(<Background selectedComponents={initObject} cabinSelectOngoing={true}  />)
    expect(wrapper.find('img').length).toEqual(5)
  })

});

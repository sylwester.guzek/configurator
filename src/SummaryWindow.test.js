import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import SummaryWindow from './SummaryWindow'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'
import { wrap } from 'module';

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<SummaryWindow />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<SummaryWindow content={initObject} open={true} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render window with itemlist', () => {
    const wrapper = shallow(<SummaryWindow content={initObject} open={true}/>)
    expect(wrapper.find('ListItem').length).toEqual(3)
  });

  it('closes modal window on clik close button', () => {
    const spy = jest.fn()
    const wrapper = shallow(<SummaryWindow content={initObject} open={true} onClose={spy} />)
    expect( wrapper.state().open ).toEqual(true)
    wrapper
      .find('Button.window-btn-close')
      .simulate('click')
    expect( wrapper.state().open ).toEqual(false)
    expect(spy).toHaveBeenCalled()
  })

  it('opens the e-mail client with pre-filled data', () => {
    const spy = jest.spyOn(SummaryWindow.prototype, 'openMail');
    const wrapper = shallow(<SummaryWindow content={initObject} open={true} />);
    wrapper.find('.window-email').simulate('click', 'using prototype');
    expect(spy).toHaveBeenCalled();
  })
});

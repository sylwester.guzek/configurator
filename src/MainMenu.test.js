import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { Icon } from 'semantic-ui-react'
import MainMenu from './MainMenu'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<MainMenu />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<MainMenu content={initObject} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render button-like Icon', () => {
    const wrapper = shallow(<MainMenu content={initObject} />)
    expect(wrapper.find(Icon).prop('name')).toEqual('bars')
  });

  it('displays menu when click-ed', () => {
    // const event = new Event('onClick')
    const wrapper = shallow(<MainMenu content={initObject} />)
    wrapper
      .find('Button')
      .simulate('click')
    expect(wrapper.state().visible).toEqual(true)
    expect(wrapper.find('SidebarLeft').length).toEqual(1)
  })

  it('passes selected layout/cabin/door to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<MainMenu content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedItem(layout[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes "cabinSelectOngoing" to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<MainMenu content={initObject} cabinSelectOngoing={spy} />)
    wrapper.instance().cabinSelectOngoing(true)
    expect(spy).toHaveBeenCalled()
  })

});
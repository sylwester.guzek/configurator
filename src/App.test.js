import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import App from './App';


describe('<App> starting window',()=> {
  it('it should work - watchdog', () => {
    expect(true).toEqual(true);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should match the snapshot', () => {
    const tree = renderer.create(<App />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should triger function on the online/ofline event', () => {
    const wrapper = shallow(<App />)
    const instance = wrapper.instance()
    const spy = jest.spyOn(instance, 'updateOnlineStatus')
    wrapper.simulate('offline')
    // wrapper.simulate('online')
    // console.log( wrapper.debug() );
    // expect(spy).toHaveBeenCalled();
  })

})





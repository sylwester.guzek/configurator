import React from 'react';
import ReactDOM from 'react-dom';
import Raven from 'raven-js';
import './index.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import ErrorBoundary from './ErrorBoundary'

Raven.config('https://13d53f74843443678b65ccafb9bc9bd7@sentry.io/1127783').install()

ReactDOM.render(
  <ErrorBoundary> 
    <App /> 
  </ErrorBoundary>, 
  document.getElementById('root'));
registerServiceWorker();

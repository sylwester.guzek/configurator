import React, { Component } from 'react'
import MainMenu from './MainMenu'
import BottomMenu from './BottomMenu'
import MobileLandscapeMenu from './MobileLandscapeMenu'
import Background from './Background'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'
import ExpandButton from './ExpandButton'
// import Snackbar from './Snackbar'
import NotificationSystem from 'react-notification-system'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //current configuration displayed on the screen
      selectedComponents : {
        selectedLayout: layout[0],
        selectedCabin: cabin[0],
        selectedDoor: door[0],      
      },
      cabinSelectOngoing: false,
      notification: "",
      }    
  }

  selectedItem = (item) => this.setState({selectedComponents: item})

  // Calculate & Update state of new dimensions
  updateDimensions = () => {
    this.setState({ winWidth: window.innerWidth, winHeight: window.innerHeight });
  }

  notificationSystem = null;

  updateOnlineStatus = (event) => {
    let condition = navigator.onLine ? "online" : "offline";
    console.log("Application is in "+condition+" mode.")
    // this.setState({notification:"Application is in "+condition+" mode." })
    this.notificationSystem.addNotification({
      message: "Application is in "+condition+" mode.",
      level: 'info',
      autoDismiss: 3,
    });
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
    window.addEventListener("online", this.updateOnlineStatus);
    window.addEventListener("offline", this.updateOnlineStatus);
    this.notificationSystem = this.refs.notificationSystem;
  }
  
  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateDimensions);
    window.removeEventListener("online", this.updateOnlineStatus);
    window.removeEventListener("offline", this.updateOnlineStatus);
  }

  cabinSelectOngoing = (active) => this.setState({cabinSelectOngoing: active});

  render() {
    let landscapeView = window.innerHeight < window.innerWidth;
    let mobileView = window.innerWidth < 768;
    let { selectedComponents, cabinSelectOngoing } = this.state;
    let iPad= /iPad/i.test(navigator.userAgent);
    return (
      <div>
        <NotificationSystem ref="notificationSystem" />
        {/* <Snackbar msg={notification || swStatus} /> */}
        {!mobileView && !iPad &&
        <div>
          <MainMenu 
            content = {this.state.selectedComponents} 
            selectedItem={this.selectedItem} 
            cabinSelectOngoing={this.cabinSelectOngoing}
            />  
          <Background 
            selectedComponents={selectedComponents}
            cabinSelectOngoing={cabinSelectOngoing}
            offset='0px'
            maxheight='100vh'
            />
        </div> }
        
        {(mobileView || iPad) && 
        <div>
          {!landscapeView && 
            <React.Fragment>
              <MainMenu 
                content = {this.state.selectedComponents} 
                selectedItem={this.selectedItem} 
                cabinSelectOngoing={this.cabinSelectOngoing}
                />  
              <Background 
                selectedComponents={selectedComponents}
                cabinSelectOngoing={cabinSelectOngoing}
                offset='60px'
                maxheight='50vh'
                />  
              <BottomMenu
                content = {this.state.selectedComponents} 
                selectedItem={this.selectedItem} 
                cabinSelectOngoing={this.cabinSelectOngoing}
                />
              <ExpandButton />
            </React.Fragment>
          }
          {landscapeView &&
            <React.Fragment> 
              <Background 
                selectedComponents={selectedComponents}
                cabinSelectOngoing={cabinSelectOngoing}
                offset='0px'
                maxheight='100vh'
                />
              <MobileLandscapeMenu
                content = {this.state.selectedComponents} 
                selectedItem={this.selectedItem} 
                cabinSelectOngoing={this.cabinSelectOngoing}
                /> 
              <ExpandButton />
            </React.Fragment>
          }
        </div>}
        
      </div>
    );
  }
}

export default App;

import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'

import ExpandButton from './ExpandButton'


describe('<ExpandButton />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<ExpandButton  />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  // TO BE SOLVED: request fullscreen object does not exist during simulaion
  it('trigers full screen function when the button is clicked', () => {
    const wrapper = shallow(<ExpandButton  />)
    const instance = wrapper.instance()
    const spy = jest.spyOn(instance, 'toggleFullScreen')

    // wrapper.find('Button').simulate('click');
    // expect(spy).toHaveBeenCalled();
  })


});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Segment, Icon, Accordion, Grid, Item } from 'semantic-ui-react';
import SummaryWindow from './SummaryWindow';

export default class SummaryButton extends Component {
  state = {
    open: false,
  }    

  open = () => this.setState({open: true})  
  close = () => this.setState({open: false})

  render() {
    return (
			<div style={{marginBottom:'-10px'}}>
				<Accordion.Title index={this.props.index} onClick={ this.open }>
          <Segment className='summary-button'>
            <Grid >
              <Grid.Column width={6}>
                {/* <Image src={activeItem.thumbnail.src} style={{height:'9em'}} centered /> */}
                <Icon size='huge' name='ordered list' className='summary-icon' />
              </Grid.Column>
              <Grid.Column width={10}>
                <Item.Content>
                  <Item.Header as='h1'> Summary	</Item.Header>
                </Item.Content>								
              </Grid.Column>
            </Grid>
					</Segment>
				</Accordion.Title>
        {this.state.open && <SummaryWindow  open content={this.props.content} onClose={this.close} /> }
      </div>
		);
  }
}

SummaryButton.propTypes  = {
  index: PropTypes.number,
  content: PropTypes.object,
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Image, Button, List, Header } from 'semantic-ui-react';

export default class SummaryWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: props.open,
    }
    this.openMail = this.openMail.bind(this)
  }

  close = () => {
    this.setState({open: false})
    this.props.onClose() 
  }

	openMail() {
		const { content } = this.props;
		let selectedItems = `Dear Sirs,%0A%0A
		I am interested in following configuration: %0A`
		for(let item in content) {
			selectedItems+=`-> ${content[item].text}%0A`
		}
    window.location.href = `mailto:user@example.com?subject=Request%20for%20offer&body=
      ${selectedItems}%0A%0A`;
  }
  
  render() {
    const { content } = this.props;
    let { open } = this.state;
		let selectedItems = '0 items selected';

		if(content) {
			selectedItems =  Object.keys(content).map( item => 
			<List.Item key={content[item].text}> {content[item].text} </List.Item>
			)
		}

    return (
        <Modal dimmer='blurring' 
          open={open} 
          onClose={this.close} 
          // trigger={<Button>Show Modal</Button>}
          >
          <Modal.Header>Configuration summary</Modal.Header>
          <Modal.Content image>
            <Image wrapped size='medium' src='/asset/molly.svg' />
            <Modal.Description>
              <Header>You have selected following items:</Header>
							<List celled ordered>
								{selectedItems} 
  						</List>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button className='window-btn-close' color='black' onClick={this.close}>
              CLOSE
            </Button>
            <Button className='window-email' positive icon='mail outline' 
              labelPosition='right' 
              content="Send E-mail" 
              onClick={this.openMail} />
          </Modal.Actions>
        </Modal>
		);
  }
}

SummaryWindow.propTypes  = {
  content: PropTypes.object,
  open: PropTypes.bool,
  onClose: PropTypes.func,
}
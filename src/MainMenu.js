import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Menu, Icon } from 'semantic-ui-react'
import SidebarLeft from './SidebarLeft'
import About from './About'

export class MainMenu extends Component {
  state = {
		visible: false,
		selectedItem: null,
		showInfo: false,
  };
  
  toggleVisibility = () => this.setState({ visible: !this.state.visible });

	selectedItem = (item) => this.props.selectedItem(item);

	cabinSelectOngoing = (active) => this.props.cabinSelectOngoing(active);
 
	handleInfo = () => {
		this.setState({showInfo: !this.state.showInfo})
	}

  render() {
		const { visible, showInfo } = this.state;
    return(
      <div className="main-layout">
        <header>
          <Menu icon inverted fixed='top' style={{ padding: '1em 1em' }}  >
						<Button 
							style={{background:'none', padding: '0em'  }}
							size='small' 
							onClick={this.toggleVisibility}
							>
								<Icon inverted name='bars' size='large' />
						</Button>
            <Menu.Header> 
							<h2> <a href="/" style={{color:'#c4c5c5'}} > Configurator </a> </h2> 
						</Menu.Header>
						<Menu.Menu position='right'>
            	<Menu.Item name='About' onClick={this.handleInfo} />
          	</Menu.Menu>
          </Menu>
        </header>
        <main>
					{ visible &&  
					<SidebarLeft 
						visible={visible}
						content={this.props.content} 
						style={{position:'absolute', top:'50px', left:'20px', zIndex:'10'}} 
						selectedItem={this.selectedItem}
						cabinSelectOngoing={this.cabinSelectOngoing}
						/>
					} 
					{showInfo && <About modalOpen modalClose={this.handleInfo} />}
        </main>
      </div>  
    );
  }
}

MainMenu.propTypes  = {
	content: PropTypes.object,
	selectedItem: PropTypes.func,
	cabinSelectOngoing: PropTypes.func,
}

export default MainMenu
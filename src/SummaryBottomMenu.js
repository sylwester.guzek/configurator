import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Segment, Button, List, Header, Divider } from 'semantic-ui-react';

export default class SummaryBottomMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      
    }
    this.openMail = this.openMail.bind(this)
  }

	openMail(){
		const { content } = this.props;
		let selectedItems = `Dear Sirs,%0A%0A
		I am interested in following configuration: %0A`
		for(let item in content) {
			selectedItems+=`-> ${content[item].text}%0A`
		}
    window.location.href = `mailto:user@example.com?subject=Request%20for%20offer&body=
      ${selectedItems}%0A%0A`;
  }
  
  render() {
    const { content } = this.props;
		let selectedItems = '0 items selected';

		if(content) {
			selectedItems =  Object.keys(content).map( item => 
			<List.Item key={content[item].text}> {content[item].text} </List.Item>
			)
		}

    return (
      <Segment inverted>
        <Header>You have selected following items:</Header>
        <Divider />
        <List >
          {selectedItems} 
        </List>
        <Divider />
        <Button color='red' onClick={this.openMail}> Send e-mail </Button>
      </Segment>
		);
  }
}

SummaryBottomMenu.propTypes  = {
  content: PropTypes.object,
}
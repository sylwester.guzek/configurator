export const door = [
  {
    key: 'Drzwi 1 Inox',
    text: 'Drzwi 1 Inox',
    value: 'Drzwi 1 Inox',
    image: {  size:'huge', src: '/asset/drzwi_1inox.svg' },
    thumbnail: { src: '/asset/thumbnail/drzwi_1inox.svg' },
  },
  {
    key: 'Drzwi 2 Inox',
    text: 'Drzwi 2 Inox',
    value: 'Drzwi 2 Inox',
    image: {  size:'huge', src: '/asset/drzwi_2inox.svg' },
    thumbnail: { src: '/asset/thumbnail/drzwi_2inox.svg' },
  },
  {
    key: 'Drzwi 3 Inox',
    text: 'Drzwi 3 Inox',
    value: 'Drzwi 3 Inox',
    image: {  size:'huge', src: '/asset/drzwi_3inox.svg' },
    thumbnail: { src: '/asset/thumbnail/drzwi_3inox.svg' },
  },
  {
    key: 'Drzwi 4 Inox',
    text: 'Drzwi 4 Inox',
    value: 'Drzwi 4 Inox',
    image: {  size:'huge', src: '/asset/drzwi_4inox.svg' },
    thumbnail: { src: '/asset/thumbnail/drzwi_4inox.svg' },
  },
  {
    key: 'Drzwi 5 Inox',
    text: 'Drzwi 5 Inox',
    value: 'Drzwi 5 Inox',
    image: {  size:'huge', src: '/asset/drzwi_5inox.svg' },
    thumbnail: { src: '/asset/thumbnail/drzwi_5inox.svg' },
  },
]

export const layout = [
  {
    key: 'Office Layout',
    text: 'Office Layout',
    value: 'Office Layout',
    image: {  size:'huge', src: '/asset/background1.svg' },
    coverWall: { size:'huge', src: '/asset/wall1.svg' },
    thumbnail: { src: '/asset/thumbnail/background1.svg' },
  },
  {
    key: 'Home Layout',
    text: 'Home Layout',
    value: 'Home Layout',
    image: {  size:'huge', src: '/asset/background3.svg' },
    coverWall: { size:'huge', src: '/asset/wall3.svg' },
    thumbnail: { src: '/asset/thumbnail/background3.svg' },
  },
  {
    key: 'Building Layout',
    text: 'Building Layout',
    value: 'Building Layout',
    image: {  size:'huge', src: '/asset/background2.svg' },
    coverWall: { size:'huge', src: '/asset/wall2.svg' },
    thumbnail: { src: '/asset/thumbnail/background2.svg' },
  },
]

export const cabin = [
  {
    key: 'Cabin yellow',
    text: 'Cabin yellow',
    value: 'Cabin yellow',
    image: {  size:'huge', src: '/asset/cabin_inside.svg' },
    thumbnail: { src: '/asset/thumbnail/cabin_inside.svg' },
  },
  {
    key: 'Cabin orange',
    text: 'Cabin orange',
    value: 'Cabin orange',
    image: {  size:'huge', src: '/asset/cabin-interno-line.svg' },
    thumbnail: { src: '/asset/thumbnail/cabin-interno-line.svg' },
  },
]

export const info = `
  This app is a demonstration of Responsive Web Design  and Progressive Web Application. 
  For best app performance on mobile use google chrome browser.
  All images are owned by http://www.metronsa.gr/.`
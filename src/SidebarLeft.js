import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Accordion, Segment, Transition, Icon, Grid } from 'semantic-ui-react'
import ItemGroup from './ItemGroup'
import SummaryButton from './SummaryButton'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'
import { forceRefresh } from './registerServiceWorker';

export default class SidebarLeft extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			animation: 'slide down', 
			duration: 500, 
			visible: false, 
		}
	}

	componentDidMount() {	
		this.setState({visible: this.props.visible})
	}

	selectedLayout = (item) => {
		let config = this.props.content;
		config.selectedLayout = Object.assign({},item);
		this.props.selectedItem(config);
	}
	selectedCabin = (item) => {
		let config = this.props.content;
		config.selectedCabin = Object.assign({},item);
		this.props.selectedItem(config);
	}
	selectedDoor = (item) => {
		let config = this.props.content;
		config.selectedDoor = Object.assign({},item);
		this.props.selectedItem(config);
	}

	handleVisibility = () => this.setState({ visible: !this.state.visible })

	cabinSelectOngoing = (active) => this.props.cabinSelectOngoing(active);

  render() {
    let landscapeView = window.innerHeight < window.innerWidth;
		let mobileView = window.innerWidth < 768 && !landscapeView;
		let iPad= /iPad/i.test(navigator.userAgent);
		const { animation, duration, visible } = this.state
    return (
			<div>
			{!(mobileView || iPad ) && 
				<Accordion styled inverted
					exclusive={false} 
					className='classic-menu' >
					<Transition.Group animation={animation} duration={duration}>
						{visible && <div>
						<ItemGroup index={0} name="Layout" 
							displayItem={this.props.content.selectedLayout} 
							itemList={layout} 
							selectedItem={this.selectedLayout} 
							/>
						<ItemGroup index={1} name="Cabin" 
							displayItem={this.props.content.selectedCabin} 
							itemList={cabin} 
							selectedItem={this.selectedCabin} 
							cabinSelectOngoing={this.cabinSelectOngoing}
							/>
						<ItemGroup index={2} name="Door" 
							displayItem={this.props.content.selectedDoor} 
							itemList={door} 
							selectedItem={this.selectedDoor} 
						/>
						<SummaryButton index={3} content={this.props.content} />		
						</div> }
					</Transition.Group>
				</Accordion>	
			}
			{(mobileView || iPad) && 
			<div className='mobile-menu'>
				<Segment raised onClick={forceRefresh} >
					<Grid >
						<Grid.Column width={4}>
							<Icon name="refresh" size="large" />
						</Grid.Column>
						<Grid.Column width={12}>
							Force Refresh
						</Grid.Column>
					</Grid>
				</Segment>
				{/* <Transition.Group animation={animation} duration={duration}>
					{ visible && 
					<div>
						<Segment inverted raised >
							Menu item placeholder - menu item placeholder.
						</Segment>
						<Segment inverted raised >
							Menu item placeholder - menu item placeholder.
						</Segment>
						<Segment inverted raised >
							Menu item placeholder - menu item placeholder.
						</Segment>
					</div>
					}
				</Transition.Group> */}
			</div>
			}
		</div>
		)
  }
}

SidebarLeft.propTypes  = {
	content: PropTypes.object,
	selectedItem: PropTypes.func,
	cabinSelectOngoing: PropTypes.func,
}
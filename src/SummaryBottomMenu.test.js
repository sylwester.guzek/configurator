import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { expect as chaiExpect}  from 'chai'
import sinon from 'sinon'

import SummaryBottomMenu from './SummaryBottomMenu'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<SummaryBottomMenu />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<SummaryBottomMenu content={initObject} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render list with 3 items', () => {
    const wrapper = mount(<SummaryBottomMenu content={initObject} />)
    expect(wrapper.find('ListItem').length).toEqual(3)  
  });

  it('trigers the e-mail client with pre-filled data', () => {
    const spy = jest.spyOn(SummaryBottomMenu.prototype, 'openMail');
    const wrapper = shallow(<SummaryBottomMenu content={initObject} />);
    wrapper.find('Button').simulate('click', 'using prototype');
    expect(spy).toHaveBeenCalled();
  })
});

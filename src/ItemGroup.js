import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Segment, Image, Grid, Accordion, Card, Item, Transition } from 'semantic-ui-react'

export default class ItemGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
			activeItem: this.props.displayItem,		
			animation: 'slide down', 
			duration: 500, 	
		};
	}

  handleClick = (e,target) => {
		const {active} = this.state
		this.setState({ active: !active })
		if(target.index===1) {
			this.props.cabinSelectOngoing(!active);
		}
	};
	
  handleItemClick = (item) => {
		this.setState({ activeItem: item });
		this.props.selectedItem(item);
  };
	
  render() {
		const { active, activeItem, animation, duration } = this.state;
		const { itemList } = this.props;
		let layoutOptionList;

		if(itemList.length) {
			layoutOptionList = itemList.map( item => 
				<Grid.Column key={item.key} > 
					<Card 
						onClick={ () => this.handleItemClick(item) }  
						className='singleItem'
						>
						<Image src={item.thumbnail.src}  />
						<Card.Content className={item.value === this.state.activeItem.value ? 'singleItemSelected' : null }>
							<Card.Header className='single-item-text'>
								{item.text}
							</Card.Header>
							<Card.Description className='single-item-text'>
								description description description description
							</Card.Description>
						</Card.Content>
					</Card>
				</Grid.Column>
			)
		} else {
			layoutOptionList = <p> No options available </p>
		}

    return (
			<div style={{marginBottom:'-10px'}}>
				<Accordion.Title 
					active={active} 
					index={this.props.index} 
					onClick={(e, target) => this.handleClick(e, target)}>
					<Segment className='item-group'>
						<Grid >
							<Grid.Column width={6}>
								<Image src={activeItem.thumbnail.src} style={{height:'9em'}} centered />
							</Grid.Column>
							<Grid.Column width={10}>
								<Item.Content>
									<Item.Header as='h1'> {this.props.name} 	</Item.Header>
									<Item.Description>
										description description
									</Item.Description>
								</Item.Content>								
							</Grid.Column>
						</Grid>
					</Segment>
				</Accordion.Title>
				<Accordion.Content active={active}>
					<Transition.Group animation={animation} duration={duration}>
						{active &&
						<Grid container columns={2} stackable >
							{layoutOptionList}
						</Grid>	}
					</Transition.Group>
				</Accordion.Content>
			</div>
		);
  }
}

ItemGroup.defaultProps = {
  itemList: [],
};

ItemGroup.propTypes  = {
	name: PropTypes.string,
	index: PropTypes.number,
	itemList: PropTypes.array,
	selectedItem: PropTypes.func,
	displayItem: PropTypes.object.isRequired,
	cabinSelectOngoing: PropTypes.func,
}
import React, { Component } from 'react'
import Raven from 'raven-js'
import { Segment, Button } from 'semantic-ui-react'

export default class ErrorBoundry extends Component {
	state = {
		hasError: false
	}
	
	componentDidCatch(error, info) {
		this.setState({ hasError: true })
		Raven.captureException(error, { extra: info });
	}
	
	render() {
		if(this.state.hasError) {
			return (
				<div className='error-boundry'>
					<Segment>
						<h2> Oh no! Somethin went wrong </h2>
						<p>Our team has been notified, but click  
							<Button  onClick={() => Raven.lastEventId() && Raven.showReportDialog()}> 
							here </Button> to fill out a report.
						</p>
					</Segment>
				</div>
			);
		} else {
			return this.props.children;
		}
	}
}
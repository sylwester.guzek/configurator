import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { expect as chaiExpect}  from 'chai'

import BottomList from './BottomList'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'
import { wrap } from 'module';

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<BottomList />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<BottomList itemList={layout} displayItem={initObject.selectedLayout} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('renders the items inside the list', () => {
    const wrapper = mount(<BottomList itemList={layout} displayItem={initObject.selectedLayout} />)
    expect(wrapper.find('Card').length).toEqual(3)
  })

  it('renders 0 items inside the list when list not provided', () => {
    const wrapper = mount(<BottomList />)
    expect(wrapper.find('Card').length).toEqual(0)
    expect(wrapper.find('.bottom-list').contains(<p> No options available </p>)).toEqual(true)
  })

  it('selects the item when you click on it', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomList itemList={layout} displayItem={initObject.selectedLayout} selectedItem={spy} />)
    wrapper.find('Card').at(1).simulate('click')
    expect(spy).toHaveBeenCalled()
    // console.log(wrapper.debug())
  })

});

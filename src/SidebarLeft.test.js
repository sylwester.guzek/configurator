import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { expect as chaiExpect}  from 'chai'

import SidebarLeft from './SidebarLeft'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<SidebarLeft />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<SidebarLeft content={initObject} visible={true} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render menu with components', () => {
    const wrapper = shallow(<SidebarLeft content={initObject} visible={true} />)
    wrapper.setState({visible: true})
    // console.log(wrapper.debug())
    chaiExpect(wrapper.find('ItemGroup')).to.have.length(3)
    chaiExpect(wrapper.find('SummaryButton')).to.have.length(1)
  });

  it('passes selected layout to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<SidebarLeft content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedLayout(layout[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes selected door to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<SidebarLeft content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedDoor(door[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes selected cabin to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<SidebarLeft content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedCabin(cabin[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes info to root element about cabin selection ongoing', () => {
    const spy = jest.fn()
    const wrapper = mount(<SidebarLeft content={initObject} cabinSelectOngoing={spy} />)
    wrapper.instance().cabinSelectOngoing(true)
    expect(spy).toHaveBeenCalled()
  })

});

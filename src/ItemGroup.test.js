import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
// import { expect as chaiExpect}  from 'chai'
import { Image, Card } from 'semantic-ui-react'
import ItemGroup from './ItemGroup'
import { layout } from './common'

describe('<ItemGroup />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<ItemGroup displayItem={layout[0]} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render button-like component with image', () => {
    const wrapper = shallow(<ItemGroup displayItem={layout[0]} />)
    expect(wrapper.find(Image).prop('src')).toEqual(layout[0].thumbnail.src)
  });

  it('displays submenu when click-ed', () => {
    const event = new Event('onClick')
    const wrapper = shallow(<ItemGroup displayItem={layout[0]} itemList={layout} />)
    const button = wrapper.find('AccordionTitle')
    expect(wrapper.state().active).toEqual(false)
    button.simulate('click', event, { target: { index: 1 }})
    expect(wrapper.state().active).toEqual(true)
  })

  it('displays sub-items', () => {
    const wrapper = shallow(<ItemGroup displayItem={layout[0]} itemList={layout} />)
    wrapper.setState({active: true})
    const items = wrapper.find('Card')
    expect(items.length).toEqual(3)
  })

  it('updates selected item on sub-item click', () => {
    const wrapper = shallow(<ItemGroup displayItem={layout[1]} itemList={layout} selectedItem={ ()=> { }} />)
    wrapper.setState({active: true})
    wrapper
      .find(Card).first()
      .simulate('click' )  // first layout=> layout[0]
    expect(wrapper.state().activeItem).toMatchObject(layout[0])
    expect(wrapper.find('AccordionTitle').find(Image).prop('src')).toEqual(layout[0].thumbnail.src)

    //alternatively test all
    const allButtons = wrapper.find(Card)
    allButtons.forEach( item => {
      item.simulate('click')
      expect(item.find(Image).prop('src')).toEqual(wrapper.state().activeItem.thumbnail.src)
    })
  })

  it('calls back the clicked item to the root component', () => {
    const sendback = jest.fn()
    const wrapper = shallow(<ItemGroup displayItem={layout[1]} itemList={layout} selectedItem={sendback} />)
    wrapper.setState({active: true})
    wrapper
      .find(Card).first()
      .simulate('click' )
    expect(sendback).toHaveBeenCalledWith(layout[0]); 
  })
  
  it('calls back the cabin selection to the root component', () => {
    const spy = jest.fn()
    const event = new Event('onClick')
    const target = { index : 1 }
    const wrapper = shallow(<ItemGroup displayItem={layout[0]} itemList={layout}
      cabinSelectOngoing={spy}  />)
    const button = wrapper.find('AccordionTitle')
    expect(wrapper.state().active).toEqual(false)
    button.simulate('click', event, target)
    expect(spy).toHaveBeenCalledWith(true); 

  })
});
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Card } from 'semantic-ui-react'

export default class BottomList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeItem: props.displayItem,		
    };
  }

  handleItemClick = (item) => {
		this.setState({ activeItem: item });
		this.props.selectedItem(item);
  };
	
  render() {
		const { itemList, orientationleft } = this.props;
		let layoutOptionList;

		if(itemList.length) {
			layoutOptionList = itemList.map( item => 
        <Card 
          onClick={ () => this.handleItemClick(item) }  
          className={orientationleft ? 'singleItem-left-menu' : 'singleItem-bottom-menu' }
          key={item.key}
          >
          <Image src={item.thumbnail.src} style={{margin: 'auto' }} />
          <Card.Content className={item.value === this.props.displayItem.value ? 'singleItemSelected' : null }>
            <Card.Header className='single-item-text'>
              {item.text}
            </Card.Header>
            <Card.Description className='single-item-text'>
              description description description description
            </Card.Description>
          </Card.Content>
        </Card>
			)
		} else {
			layoutOptionList = <p> No options available </p>
		}

    return (
			<div className ={ orientationleft ? 'left-list' : 'bottom-list' } >
        {layoutOptionList}
			</div>
		);
  }
}

BottomList.defaultProps = {
  itemList: [],
  orientationleft: false,
};

BottomList.propTypes  = {
  orientationleft: PropTypes.bool,
  itemList: PropTypes.array,
  displayItem: PropTypes.object,
	selectedItem: PropTypes.func,
	cabinSelectOngoing: PropTypes.func,
}
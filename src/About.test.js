import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'

import About from './About'

describe('<About />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<About modalOpen />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('window closes on button ok click ', () => {
    const spy = jest.fn()
    const wrapper = shallow(<About modalOpen  modalClose={spy}/>)
    wrapper
      .find('Button')
      .simulate('click')
    expect(spy).toHaveBeenCalled();
  })


});

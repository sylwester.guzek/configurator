import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'
import { version } from '../package.json'
import { info } from './common'

export default class About extends Component {
  state = { modalOpen: this.props.modalOpen }
  handleClose = () => {
    this.setState({ modalOpen: false })
    this.props.modalClose();
  }

  render() {
    return (
      <Modal
        // trigger={<Button onClick={this.handleOpen}>Show Modal</Button>}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
        size='small'
      >
        <Header>
          <Icon name="info circle" size="huge" />
          About
        </Header>
        <Modal.Content>
          <p> {info} </p>
          <br /> App version {version}
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.handleClose} inverted>
            <Icon name='checkmark' /> OK
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

About.propTypes  = {
  modalOpen: PropTypes.bool,
  modalClose: PropTypes.func,
}


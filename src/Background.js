import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Background extends Component  {
  render() {
    const { offset, maxheight, selectedComponents, cabinSelectOngoing } = this.props
    return (
      <div style={{ position:'relative',  height: maxheight}} >
        <img src={selectedComponents.selectedLayout.image.src} 
          className='background'
          style={{ top: offset }}
          alt=""   />
        <img src={selectedComponents.selectedCabin.image.src} 
          className='background'
          style={{ top: offset }}
          alt=""   />
        <img src="/asset/cabin_top.svg" 
          className='background'
          style={{ top: offset }}
          alt=""   />
        <img src="/asset/frame.svg" 
          alt=""    
          className='background'
          style={{ top: offset }}/>
        <img src={selectedComponents.selectedDoor.image.src} 
          className='background'
          style={{ top: offset }}
          alt=""   />
        {!cabinSelectOngoing && 
          <img src={selectedComponents.selectedLayout.coverWall.src} 
          className='background'
          style={{ top: offset }}
          alt=""   />
          }
      </div>
    )
  }
}

Background.defaultProps = {
  cabinSelectOngoing: false,
  offset: '0px',
  maxheight: '100vh',
}

Background.propTypes  = {
  selectedComponents: PropTypes.object.isRequired,
  cabinSelectOngoing: PropTypes.bool,
  offset: PropTypes.string,
  maxheight: PropTypes.string,
}
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

export default class ExpandButton extends Component {
  state = {
    fullScreen: false,
  }    

  toggleFullScreen = () => {
    var doc = window.document;
    var docEl = doc.documentElement;
  
    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
    
    console.log(requestFullScreen);
    
    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
      requestFullScreen.call(docEl);
      this.setState({fullScreen: true})
    }
    else {
      cancelFullScreen.call(doc);
      this.setState({fullScreen: false})
    }
  }

  componentDidMount() {
    var doc = window.document;
    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
      this.setState({fullScreen: false})
    } else {
      this.setState({fullScreen: true})
    }
  }

  render() {
    let { fullScreen } = this.state;
    let iPad= /iPad/i.test(navigator.userAgent);
    let iPhone= /iPhone/i.test(navigator.userAgent);
    return (
      <React.Fragment>
      {!( iPhone || iPad) && <Button circular
        className="full-screen-button"  
        color='blue' 
        icon={ fullScreen ? 'minimize' : 'maximize' }
        size='massive'
        onClick={this.toggleFullScreen} 
        />}
      </React.Fragment>
		);
  }
}

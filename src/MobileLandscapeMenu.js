import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Tab, Menu } from 'semantic-ui-react'
import BottomList from './BottomList'
import SummaryBottomMenu from './SummaryBottomMenu'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'


export default class MobileLandscapeMenu extends Component {
  state = {

  }
  selectedLayout = (item) => {
		let config = this.props.content;
		config.selectedLayout = Object.assign({},item);
		this.props.selectedItem(config);
	}
	selectedCabin = (item) => {
		let config = this.props.content;
		config.selectedCabin = Object.assign({},item);
		this.props.selectedItem(config);
	}
	selectedDoor = (item) => {
		let config = this.props.content;
		config.selectedDoor = Object.assign({},item);
		this.props.selectedItem(config);
  }
  tabChange = (event, data) => {
    if(data.activeIndex===1) 
      this.props.cabinSelectOngoing(true)
    else
      this.props.cabinSelectOngoing(false)
  }

  render() {
    const panes = [
      { menuItem: 'Layout', render: () => 
        <Tab.Pane 
          // attached='false' 
          className = 'mobile-landscape-menu'> 
          <BottomList 			
            orientationleft				
            displayItem={this.props.content.selectedLayout} 
            itemList={layout} 
            selectedItem={this.selectedLayout} 
            />
        </Tab.Pane> 
      },
      { menuItem: 'Cabin', render: () => 
        <Tab.Pane 
          // attached='false' 
          className = 'mobile-landscape-menu'> 
          <BottomList 		
            orientationleft					
            displayItem={this.props.content.selectedCabin} 
            itemList={cabin} 
            selectedItem={this.selectedCabin}
            />
        </Tab.Pane> 
      },
      { menuItem: 'Door', render: () => 
        <Tab.Pane 
          // attached='false' 
          className = 'mobile-landscape-menu'>
          <BottomList 			
            orientationleft				
            displayItem={this.props.content.selectedDoor} 
            itemList={door} 
            selectedItem={this.selectedDoor}
            />
        </Tab.Pane> 
      },
      { menuItem: 'Summary', render: () => 
      <Tab.Pane 
      // attached='false' 
      className = 'mobile-landscape-menu'>
        <SummaryBottomMenu orientationleft content={this.props.content} />
      </Tab.Pane> 
    },
    ]
    return (
      <Menu  fixed='bottom' inverted className='mobile-landscape-menu-bar' >
        <Tab 
          onTabChange={this.tabChange}
          menu={{ color:'black', inverted: true, attached: 'bottom'}} 
          panes={ panes } 
          // style={{backgroundColor:'transparent'}}
          />
      </Menu>
    )
  }  
}


MobileLandscapeMenu.propTypes  = {
	content: PropTypes.object,
	selectedItem: PropTypes.func,
	cabinSelectOngoing: PropTypes.func,
}


import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { expect as chaiExpect}  from 'chai'

import BottomMenu from './BottomMenu'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'

let initObject = {
  selectedLayout: layout[0],
  selectedCabin: cabin[0],
  selectedDoor: door[0], 
}

describe('<BottomMenu />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<BottomMenu content={initObject} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render bottom menu tabs x4', () => {
    const wrapper = mount(<BottomMenu content={initObject} />)
    // console.log(wrapper.debug())
    chaiExpect(wrapper.find('MenuItem')).to.have.length(4);
  });

  it('changes the tab when clicked on it', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} cabinSelectOngoing={spy} />)
    let tab = wrapper.find('MenuItem').at(1).simulate('click')
    expect(wrapper.find('Menu').at(1).prop('activeIndex')).toEqual(1)
  });

  it('renders the items inside the active tab - default tab', () => {
    const wrapper = mount(<BottomMenu content={initObject} />)
    expect(wrapper.find('Card').length).toEqual(3)
  })

  it('renders "door" tab', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} cabinSelectOngoing={spy}/>)
    let tab = wrapper.find('MenuItem').at(2).simulate('click')
    expect(wrapper.find('Card').length).toEqual(5)
  })

  it('renders "summary" tab', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} cabinSelectOngoing={spy}/>)
    let tab = wrapper.find('MenuItem').at(3).simulate('click')
    expect(wrapper.find('SummaryBottomMenu').length).toEqual(1)
  })
  
  it('passes selected layout to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedLayout(layout[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes selected door to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedDoor(door[0])
    expect(spy).toHaveBeenCalled()
  })

  it('passes selected cabin to root component', () => {
    const spy = jest.fn()
    const wrapper = mount(<BottomMenu content={initObject} selectedItem={spy} />)
    wrapper.instance().selectedCabin(cabin[0])
    expect(spy).toHaveBeenCalled()
  })

});

import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import { expect as chaiExpect}  from 'chai'

import { Icon, AccordionTitle } from 'semantic-ui-react'
import SummaryButton from './SummaryButton'
import { layout } from './common'
import { door } from './common'
import { cabin } from './common'
import { wrap } from 'module';



describe('<SummaryButton />', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<SummaryButton />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render button like assertion component with icon and label "Summary"', () => {
    const wrapper = shallow(<SummaryButton index={1} />)
    chaiExpect(wrapper.find('Icon')).to.have.length(1)
  });

  it('trigers modal window to open', () => {
    const wrapper = shallow(<SummaryButton index={1} />)
    expect(wrapper.state().open).toEqual(false)
    wrapper
      .find('AccordionTitle')
      .simulate('click')
    expect(wrapper.state().open).toEqual(true)
  })

  it('trigers modal window to close', () => {
    const spy = jest.fn()
    const wrapper = shallow(<SummaryButton index={1} />)
    wrapper
      .setState({open: true})
      .find('SummaryWindow')
      .simulate('close')
    expect(wrapper.state().open).toEqual(false)
  })

});
  
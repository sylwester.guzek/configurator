import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import { expect as chaiExpect}  from 'chai'
import sinon from 'sinon'
import ErrorBoundary from './ErrorBoundary'

class ComponentWithError extends Component {
  render() {
    return (
      <div>
        <p> Error testing component </p>
        <input type = "text" value = {null}/>
      </div>
    );
  }
}

function ProblemChild() {
  throw new Error('Error thrown from problem child');
  return <div>Error</div>; // eslint-disable-line
}

describe('<ErrorBoundary> window',()=> {
  it('should match the snapshot', () => {
    const tree = renderer.create(<ErrorBoundary>Test</ErrorBoundary> ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('displays error message on error generated by child', () => {
    const spy = sinon.spy(ErrorBoundary.prototype, 'componentDidCatch')
    mount(<ErrorBoundary><ProblemChild /></ErrorBoundary>)
    chaiExpect(ErrorBoundary.prototype.componentDidCatch).to.have.property('callCount', 1)
  })
})
